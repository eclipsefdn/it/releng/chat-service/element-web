(import 'chat-elementweb/main.libsonnet') +
{
  _config+:: {
    environment: 'prod',
    elementweb+: {
      replicas: 2,
      version: 'v1.11.94',
      config+: {
        redirectRegistrer: 'https://accounts.eclipse.org/user/register?destination=user',
      },
    },
  },
}
